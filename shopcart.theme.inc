<?php
// $Id: shopcart.theme.inc,v 1.3 2011/09/12 14:30:24 dhavyd Exp $
/*
 * @file
 * shopcart.theme.inc Contains theme-functions
 */

/**
 * Theme the list of nodes in cart.
 * Will be used for block which shows cart content.
 *
 * @param array $list Associated array when key is nid and value - some node's information.
 * @param boolean $show_remove_link TRUE if remove link should be shown.
 * @param boolean $show_add_link TRUE if add link should be shown.
 * @param boolean $show_item_quantity TRUE if item's quantity should be shown.
 * @return string Rendered list of nodes in cart.
 */
function theme_cart_list($list=array()) {
  $output = '';
  $formatted = array();
  $total_price = 0;
  $attributes = array('id' => 'cart-list-block');
  $cart_is_empty = TRUE;
  $hide = 'style="display:none"';
  $rendered_items_list = '<div class="item-list"><ul id="cart-list-block"></ul></div>';
  if (is_array($list['items']) && count($list['items']) > 0) {
    $cart_is_empty = FALSE;
    foreach ($list['items'] as $nid => $node_data) {
      $formatted[] = theme('cart_block_item', $node_data, $use_destination);
    }
    $total_price = check_plain($list['total_price']);
    $rendered_items_list = theme('item_list', $formatted, NULL, 'ul', $attributes);
  }
  $output .= '<span ' . ($cart_is_empty ? '' : $hide) . ' id="cart-block-empty-message">' 
    . t('Your cart is empty') . '.</span>' . $rendered_items_list
    . '<div ' . ($cart_is_empty ? $hide : '') . ' id="cart-block-total"><strong>'
    . t('Total:') . ' <span id="cart-block-total-price">'
    . format_number($total_price, 2) . '</span> ' . variable_get('shopcart_admin_currency', '') . '</strong></div>';
  
  $shipping_fee = variable_get('shopcart_admin_shipping_fee', FALSE);
  $currency = variable_get('shopcart_admin_currency', 'USD');
  
  if ($shipping_fee) { 
    $output .= '<div class="shipping-text" ' . ($cart_is_empty ? $hide : '') . '>' . t('Excludes shipping') . ': ' . format_number($shipping_fee, 2) . ' ' . $currency . '</div>';
  }
  
  $edit_cart_link = variable_get('shopcart_block_edit_cart_link', FALSE);
  if ($edit_cart_link) {
    $output .= l(t('Edit cart'), 'shopcart', array('attributes' => array('class' => 'cart-action-button cart-button')));
  }
  $output .= l(t('Checkout'), 'shopcart/checkout', array('attributes' => array('class' => 'cart-action-button cart-button')));
  
  return $output;
}

/**
 * Theme cart's item for block with items list.
 */
function theme_cart_block_item($node_data=array()) { 
  $output = '';
  $price = $node_data['price'];
  if (count($node_data) > 0) {
    if (variable_get('shopcart_block_show_item_quantity', TRUE)) {
      $output .= '<div class="cart-item-title"><span class="cart-item-quantity">'
                 . check_plain($node_data['quantity']) . '</span> st ';
    }
    $output .= l($node_data['title'], 'node/' . $node_data['nid'],
      array(
        'attributes' => array(
          'class' => 'cart-list_block-item',
          'id'    => 'cart-block-item-' . check_plain($node_data['nid'])
        )
      )
    );
    $output .= '</div>';
    $params = shopcart_split_params($node_data['params']);
    
    
    if (array_count_values($params) > 0) {
      foreach ($params as $key => $value) {
        $params_title_value = shopcart_plugins_func_get_value($key, $value);
        
        
        if ($params_title_value['price']) {
          $price = $params_title_value['price']['value'];
        }
        else {
          $output .= '<div class="cart-item-extras cart-item-extras-' . $key . '"><span class="cart-item-extras-title">' . $params_title_value[$key]['title'] . ': </span><span class="cart-item-extras-value">' . $params_title_value[$key]['value'] . '</span></div>';
        }
      }
    }
    $output .= '<div class="cart-item-sum">' . t('Sum') . ": " . $price * check_plain($node_data['quantity']) . ' ' . variable_get('shopcart_admin_currency', '') . '</div>';
    $output .= theme('cart_links', $node_data['nid'], $node_data['quantity'], $node_data['params'], $use_destination);
  }
  
  return $output;
}

/**
 * Theme 'Remove From Cart' links.
 *
 * @param integer $nid Nodes ID.
 * @param boolean $show_remove_link TRUE if 'Remove From Cart' link should be added.
 * @param boolean $show_add_link TRUE if 'Add To Cart' link should be added.
 * @return string Returns rendered HTML with requested buttons code.
 */
function theme_cart_links($nid='', $quantity=1, $params='0') {  
  if ($nid) {
    $destination = drupal_get_destination();
    $links = array();
    // add a remove link
    if (variable_get('shopcart_block_show_remove_link', TRUE)) {
      $links[] = l(t('Remove'), 'shopcart/remove/' . $nid . "/0/" . $params,
        array(
          'query' => $destination,
          'attributes' => array('class' => 'cart-action-button')))
        . '<span style="display:none" class="shopcart-throbber">'. t('Working...') . '</span>';
    }
    if (count($links)) {
      return ' ' . implode('', $links);
    }
  }
}

/**
 * Theme for cart items form. Shows form as a table with checkboxes.
 *
 * @param array $form
 * @return string Rendered form
 */
function theme_cart_form($form) {
  $path = drupal_get_path('module', 'shopcart');
  drupal_add_css($path . '/shopcart.css');
  $rows = array();
  $type_names = node_get_types('names');
  $type = '';
  foreach (element_children($form['checkboxes']) as $nid) {
    $row = array();
    if ($type != $form[$nid]['node_type']['#value']) {
      $type = $form[$nid]['node_type']['#value'];
      // Insert subheader:
      $rows[] = array(
        'data' => array(
          array('data' => $type_names[$type], 'colspan' => 5, 'class' => 'node-type-subheader'),
        ),
      );
    }
    $row[] = drupal_render($form['checkboxes'][$nid]);
    $row[] = drupal_render($form[$nid]['teaser']);
    $row[] = array(
      'data' => drupal_render($form[$nid]['price']) . ' ' . variable_get('shopcart_admin_currency', ''),
      'class' => 'price',
    );
    $row[] = drupal_render($form['quantity']['quantity_' . $nid]);

    $row[] = array(
      'data' => drupal_render($form[$nid]['item_amount']) . ' ' . variable_get('shopcart_admin_currency', ''),
      'class' => 'item-amount',
    );
    $rows[] = $row;
  }
  if (count($rows)) {
    $remove_all_header = theme('table_select_header_cell');
    $header = array($remove_all_header, t('Items'), t('Item Price'), t('Quantity'), t('Amount'));
    $rows[] = array(
      'data' => array(
        array('data' => t('Total'), 'colspan' => 4, 'class' => 'total_price_label'),
        array('data' => drupal_render($form['total_price']) . ' ' . variable_get('shopcart_admin_currency', ''), 'class' => 'total_price_amount'),
      ),
    );
    $output = theme('table', $header, $rows);
    return $output . drupal_render($form);
  }
  else {
    $output = '';
    $output .= '<p class="empty-cart">' . t('Your cart is empty') . '.</p>';
    $output .= '<input ' . drupal_attributes(array(
        'onClick' => 'javascript:window.location="' . variable_get('shopcart_admin_shop_path', '') . '";',
        'class' => 'form-submit',
        'type' => 'button',
        'name' => 'backtoshop',
        'value' => t('Back to shop'))) . '/>';
    return $output;
  }
}



/**
 * Theme for cart items form. Shows form as a table with checkboxes.
 *
 * @param array $form
 * @return string Rendered form
 */
function theme_checkout_cart_items($list) {
  
  if (count($list) > 0) {
    
    $path = drupal_get_path('module', 'shopcart');
    drupal_add_css($path . '/shopcart.css');
    $rows = array();
    foreach ($list as $nid => $node_data) {
      $pos = strpos($nid, "_");
      $nid_id = substr($nid, 0, $pos);
      $params = $list[$nid]['params'];
      $node = node_load($nid_id);
      $node->build_mode = 'checkout';
      $row = array();
      
      $pos = strpos($node_data['title'], 'Shipping');

      if ($pos !== FALSE) {
        $row[] = t($node_data['title']);
      }
      else {
        $row[] = shopcart_get_node_teaser($node, $params);
      }
      
      $row[] = check_plain($node_data['quantity']);
      $row[] = check_plain($node_data['price']) . ' ' . variable_get('shopcart_admin_currency', '');
      
      $total_price += $node_data['price'] * $node_data['quantity'];
      $rows[] = $row;
    }

    if (count($rows)) {
      
      $shipping_fee = variable_get('shopcart_admin_shipping_fee', FALSE);

      if ($shipping_fee) {
        $row = array();
        $row[] = '<div class="order-section">' . t('Shipping fee') . '</div>';
        $row[] = '<div class="order-section">' . check_plain(1) . '</div>';
        $row[] = '<div class="order-section">' . check_plain(format_number($shipping_fee, 2)) . ' ' . variable_get('shopcart_admin_currency', '') . '</div>';
        $total_price += $shipping_fee;
        $rows[] = $row;
      }

      $header = array(t('Items'), t('Quantity'), t('Price per unit'));
      $rows[] = array(
        'data' => array(
          array('data' => t('Total'), 'colspan' => 2, 'class' => 'total_price_label'),
          array('data' => format_number($total_price, 2) . ' ' . variable_get('shopcart_admin_currency', ''), 'class' => 'total_price_amount'),
        ),
      );
      
      return theme('table', $header, $rows);
    }
    else {
      return '<p class="empty-cart">' . t('The cart is empty') . '</p>';
    }
  }
}


/**
 * Theme for cart items form. Shows form as a table with checkboxes.
 *
 * @param array $list Array of nodes in cart. Key is a node's ID and value contain some node's data.
 * @param string $build_mode Node build mode. Can be 'customer_email' or 'manager_email'. But actualy can have other values 'teaser', 'full', 'rss' and etc.
 * @return string Rendered list of nodes in cart.
 */
function theme_email_order_confirmation($order=array()) {
  $list = shopcart_content_list('checkout');
  $output = '';
  $order_sum = 0;

//  $output .= '
//    <html>
//    <head></head>
//    <body>
//      <b>' . variable_get('shopcart_order_title_succeed', '') . '</b>
//      <p>' . variable_get("shopcart_order_email_customer", "") . '</p>
//      <table>
//        <tr>
//          <td><b>' . t("ID") . '</b></td>
//          <td><b>' . t("Title") . '</b></td>
//          <td><b>' . t("Price") . '</b></td>
//          <td><b>' . t("Quantity") . '</b></td>
//          <td><b>' . t("Total") . '</b></td>
//        </tr>
//    ';
//
//  if (count($order['items']) > 0) {
//
//    foreach ($order['items'] as $key => $value) {
//
//      $order_item_total = $value['price'] * $value['quantity'];
//      $order_sum = $order_sum + $order_item_total;
//
//      $output .= '
//        <tr>
//          <td>' . $value['productid'] . '</td>
//          <td><b>' . $value['title'] . '</b>';
//
//      // Handle the params
//      $params = shopcart_split_params($value['params']);
//      if (array_count_values($params) > 0) {
//        foreach ($params as $key2 => $value2) {
//          if ($key2 != 'price') {
//            // Print the params with title and value
//            $params_title_value = shopcart_plugins_func_get_value($key2, $value2);
//            $output .= '<br />' . $params_title_value[$key2]['title'] . ': ' . $params_title_value[$key2]['value'];
//          }
//        }
//      }
//
//      $output .= '</td>
//          <td>' . format_number($value['price'], 2) . '</td>
//          <td>' . $value['quantity'] . '</td>
//          <td>' . format_number($order_item_total, 2) . '</td>
//        </tr>
//      ';
//    }
//  }
//  $output .= '
//        <tr>
//          <td></td>
//          <td></td>
//          <td></td>
//          <td><b>' . t('Sum') . ':</b></td>
//          <td>' . format_number($order_sum, 2) . '</td>
//        </tr>
//      </table>
//      </body>
//      </html>';
  
  
  $order_title_succeed = variable_get('shopcart_order_title_succeed', FALSE);
  $order_email_customer = variable_get('shopcart_order_email_customer', FALSE);
  
  if ($order_title_succeed) {
    $output[] = $order_title_succeed;
    $output[] = "\n";
  }
  if ($order_email_customer) {
    $output[] = $order_email_customer;
    $output[] = "\n";
  }
  
  $output[] = t('Order number') . ': ' . $order['id'];
  $output[] = t('Status') . ': ' . shopcart_get_status_text($order['status']);
  
  if (count($order['items']) > 0) {
    foreach ($order['items'] as $key => $value) {
      $order_item_total = $value['price'] * $value['quantity'];
      $order_sum = $order_sum + $order_item_total;

      $output[] = '====================';
      // Check if order item is shipping fee
      
      $pos = strpos($value['params'], 'shipping');
      if ($pos !== FALSE) {
        $output[] = t('Shipping fee');
        $params = array();
      }
      else {
        $output[] = $value['productid'] . ' - ' . $value['title'];
        $params = shopcart_split_params($value['params']);
      }      
      // Handle the params
      if (array_count_values($params) > 0) {
        foreach ($params as $key2 => $value2) {
          if ($key2 != 'price') {
            // Print the params with title and value
            $params_title_value = shopcart_plugins_func_get_value($key2, $value2);
            $output[] = $params_title_value[$key2]['title'] . ': ' . $params_title_value[$key2]['value'];
          }
        }
      }
      $output[] = t('Price') . ': ' . format_number($value['price'], 2) . ' ' . variable_get('shopcart_admin_currency', '');
      $output[] = t('Quantity') . ': ' . $value['quantity'];
      $output[] = t('Total') . ': ' . format_number($order_item_total, 2) . ' ' . variable_get('shopcart_admin_currency', '');

    }
    $output[] = '====================';
    // Total amount
    $output[] = t('Sum') . ': ' . format_number($order_sum, 2) . ' ' . variable_get('shopcart_admin_currency', '');
    // VAT
    $vat = shopcart_get_vat($order_sum);
    if ($vat) {
      $output[] = t('VAT included') . ': ' . format_number($vat, 2) . ' ' . variable_get('shopcart_admin_currency', '');
    }
    $output[] = "\n";

    if ($order['message']) {
      $output[] = t('Message to') . ' ' . variable_get('site_name', 'shop');
      $output[] = $order['message'];
      $output[] = "\n";
    }
    
    $output[] = t('Buyer');
    
    $output[] = t('Firstname') . ": " . $order['buyer']['firstname'];
    $output[] = t('Lastname') . ": " . $order['buyer']['lastname'];
    $output[] = t('Company') . ": " . $order['buyer']['company'];
    $output[] = t('Address') . ": " . $order['buyer']['address'];
    $output[] = t('Zip') . ": " . $order['buyer']['zip'];
    $output[] = t('Postal') . ": " . $order['buyer']['postal'];
    $output[] = t('Country') . ": " . $order['buyer']['country'];
    $output[] = t('Phone') . ": " . $order['buyer']['phone'];
    $output[] = t('E-mail') . ": " . $order['buyer']['email'];
    $output[] = "\n";

    if ($order['receiver']['firstname'] || $order['receiver']['company']) {
      $output[] = t('Receiver');
      $output[] = t('Firstname') . ": " . $order['receiver']['firstname'];
      $output[] = t('Lastname') . ": " . $order['receiver']['lastname'];
      $output[] = t('Company') . ": " . $order['receiver']['company'];
      $output[] = t('Address') . ": " . $order['receiver']['address'];
      $output[] = t('Zip') . ": " . $order['receiver']['zip'];
      $output[] = t('Postal') . ": " . $order['receiver']['postal'];
      $output[] = t('Country') . ": " . $order['receiver']['country'];
      if ($order['message_receiver']) {
        $output[] = t('Message to receiver');
        $output[] = $order['message_receiver'];
      }
      $output[] = "\n";
    }
    
    $output[] = '--------------------';

    
    $output[] = variable_get('shopcart_order_email_customer_foot', '');
        
  }
  
  return implode("\n", $output);
}


function theme_add_to_cart_link($node) {
  $button_anchor = variable_get('shopcart_add_to_cart_button_text' . $node->type, t('Add to Cart'));
  $output = l($button_anchor, 'shopcart/add/' . $node->nid . '/1/0',
    array('query' => drupal_get_destination(), 'attributes'  => array('class' => 'form-submit'))
  );
  $output .= '<span style="display:none" class="shopcart-throbber">'. t('Working...') . '</span>';
  
  return $output;
}


/**
 * Theme for cart items form. Shows form as a table with checkboxes.
 *
 * @param array $form
 * @return string Rendered form
 */
function theme_showorder($order) {
  
  $order_sum = 0;
  $output = '';
  
  $output .= '<div id="order-details">';
  $output .= '<div class="order-id"><span >' . t('Order ID') . ': </span>' . $order['id'] . '</div>';
  $output .= '<div class="order-status"><span >' . t('Status') . ': </span>' . shopcart_get_status_text($order['status']) . '</div>';
  $output .= '<div >
    <table class="order-items">
      <thead>
        <tr>
          <th>' . t('Product ID') . '</th>
          <th>' . t('Title') . '</th>
          <th>' . t('Price per unit') . '</th>
          <th>' . t('Quantity') . '</th>
          <th>' . t('Total') . '</th>
        </tr>
      </thead>
      <tbody>
    ';
    
  // Get all items in the order
  foreach ($order['items'] as $key => $value) {
    
    $order_item_total = $value['price'] * $value['quantity'];
    $style = '';
    // Handle the params
    if ($value['params'] != 'shipping') {
      $params = shopcart_split_params($value['params']);
    }
    else {
      $value['title'] = t('Shipping fee');
      $style .= ' order-section';
      $params = array();
    }
    $output .= '
      <tr>
        <td class="' . $style . '">' . $value['productid'] . '</td>
        <td class="' . $style . '"><span class="order-item-title">' . $value['title'] . '</span>';
    
    if (array_count_values($params) > 0) {
      foreach ($params as $key2 => $value2) {
        if ($key2 != 'price') {
          // Print the params with title and value
          $params_title_value = shopcart_plugins_func_get_value($key2, $value2);
          $output .= '<br />' . $params_title_value[$key2]['title'] . ': ' . $params_title_value[$key2]['value'];
        }
      }
    }
    $output .= '</td>
        <td class="' . $style . '">' . format_number($value['price'], 2) . ' ' . variable_get('shopcart_admin_currency', '') . '</td>
        <td class="' . $style . '">' . $value['quantity'] . '</td>
        <td class="' . $style . '">' . format_number($order_item_total, 2) . ' ' . variable_get('shopcart_admin_currency', '') . '</td>
      </tr>';
    
    $order_sum = $order_sum + $order_item_total;
  }
  // Total amount
  $output .= '
      <tr>
        <td class="order-sum order-section">' . t('Sum') . '</td>
        <td class="order-sum order-section"></td>
        <td class="order-sum order-section"></td>
        <td class="order-sum order-section"></td>
        <td class="order-sum order-section">' . format_number($order_sum, 2) . ' ' . variable_get('shopcart_admin_currency', '') . '</td>
      </tr>';
  
  // VAT
  $vat = shopcart_get_vat($order_sum);
  if ($vat) {
    $output .= '
      <tr>
        <td class="order-vat order-section">' . t('VAT included') . '</td>
        <td class="order-vat order-section"></td>
        <td class="order-vat order-section"></td>
        <td class="order-vat order-section"></td>
        <td class="order-vat order-section">' . format_number($vat, 2) . ' ' . variable_get('shopcart_admin_currency', '') . '</td>
      </tr>';
  }
  
  $output .= '</tbody></table></div>';
  
  
  
  // Buyer
  
  $output .= '<div class="block-buyer"><span class="order-block-title">' . t('Buyer') . '</span>
    <table >
      <tr>
        <td class="order-title-column">' . t('Firstname') . ':</td>
        <td>' . $order['buyer']['firstname'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Lastname') . ':</td>
        <td>' . $order['buyer']['lastname'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Company') . ':</td>
        <td>' . $order['buyer']['company'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Address') . ':</td>
        <td>' . $order['buyer']['address'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Zip') . ':</td>
        <td>' . $order['buyer']['zip'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Postal') . ':</td>
        <td>' . $order['buyer']['postal'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Country') . ':</td>
        <td>' . $order['buyer']['country'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Phone') . ':</td>
        <td>' . $order['buyer']['phone'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('E-mail') . ':</td>
        <td>' . $order['buyer']['email'] . '</td>
      </tr>
    </table></div>';

  
  if (!empty($order['receiver']['firstname'])) {

    // Receiver

    $output .= '<div class="block-receiver"><span class="order-block-title">' . t('Receiver') . '</span>
    <table >
      <tr>
        <td class="order-title-column">' . t('Firstname') . ':</td>
        <td>' . $order['receiver']['firstname'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Lastname') . ':</td>
        <td>' . $order['receiver']['lastname'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Company') . ':</td>
        <td>' . $order['receiver']['company'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Address') . ':</td>
        <td>' . $order['receiver']['address'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Zip') . ':</td>
        <td>' . $order['receiver']['zip'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Postal') . ':</td>
        <td>' . $order['receiver']['postal'] . '</td>
      </tr>
      <tr>
        <td class="order-title-column">' . t('Country') . ':</td>
        <td>' . $order['receiver']['country'] . '</td>
      </tr>
    </table></div>';
    $output .= '<div class="order-delivery-address">' . t("Delivery address") . ': ' .  t("Same as receiver") . '</div>';
  }
  else {
    $output .= '<div class="order-delivery-address">' . t("Delivery address") . ': ' . t("Same as buyer") . '</div>';
  }

  $output .= '</div>';
  
  return $output;
  
  
}



/**
 * Theme for cart items form. Shows form as a table with checkboxes.
 *
 * @param array $form
 * @return string Rendered form
 */
function theme_order_error($values) {
  $output = '';
  $output .= '<div class="order-handler-orderid">' . t('Order ID') . ': ' . $values['orderid'] . '</div>';

  $order_introduction_fail = variable_get('shopcart_order_introduction_fail', FALSE);
  if ($order_introduction_fail) {
    $output .= '<div class="order-handler-introduction">' . $order_introduction_fail . '</div>';
  }
  $order_body_fail = variable_get('shopcart_order_body_fail', FALSE);
  if ($order_body_fail) {
    $output .= '<div class="order-handler-body">' . $order_body_fail . '</div>';
  }  
  
  // If the shop path is set, show the "Back to shop" button
  $shop_path = variable_get('shopcart_admin_shop_path', '');
  if (!empty($shop_path)) {
    $output .= '
        <a class="button-link" href="' . base_path() . $shop_path . '">' . t('Back to shop') . '</a>
      ';
  }
  // If the path to the customer service page is set, show the "Contact the customer service" button
//  $customer_service = variable_get('shopcart_admin_customer_service', '');
//  if (!empty($customer_service)) {
//    $output .= '
//        <a class="button-link" href="' . base_path() . $customer_service . '">' . t('Contact the customer service') . '</a>
//      ';
//  }
  
  return $output;
}


/**
 * Theme for cart items form. Shows form as a table with checkboxes.
 *
 * @param array $form
 * @return string Rendered form
 */
function theme_order_confirmation_text($text) {
  $output = '';
  $output .= '
      <div class="order-confirmation-text">' . $text . '</div>
    ';
  return $output;
}





