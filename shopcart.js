
$(document).ready(function() {

  // Get default country
  default_country = $('#edit-country-receiver').val();
  
  // Action if receiver is same as buyer (copy fields to receiver fields)
  $('#edit-receiver-radios-0').click(function() {
    $('#edit-firstname-receiver').val($('#edit-firstname').val());
    $('#edit-lastname-receiver').val($('#edit-lastname').val());
    $('#edit-company-receiver').val($('#edit-company').val());
    $('#edit-address-receiver').val($('#edit-address').val());
    $('#edit-zip-receiver').val($('#edit-zip').val());
    $('#edit-postal-receiver').val($('#edit-postal').val());
    $('#edit-country-receiver').val($('#edit-country').val());
  });

  // Action if receiver is other than buyer (clean all receiver fields)
  $('#edit-receiver-radios-1').click(function() {
    $('#edit-firstname-receiver').val('');
    $('#edit-lastname-receiver').val('');
    $('#edit-company-receiver').val('');
    $('#edit-address-receiver').val('');
    $('#edit-zip-receiver').val('');
    $('#edit-postal-receiver').val('');
    $('#edit-country-receiver').val(default_country);
  });

});


