For more information about this module please visit http://drupal.org/project/shopcart

Contents of the file
---------------------

 * About Drupal
 * Overview
 * Requirements
 * Installing Shopcart
 * Use Shopcart
   - Create product categories
   - Create products
   - Administer Shopcart
 

*** About Drupal
------------
Drupal is an open source content management platform supporting a variety of
websites ranging from personal weblogs to large community-driven websites. For
more information, see the Drupal website at http://drupal.org/, and join the
Drupal community at http://drupal.org/community.

Legal information about Drupal:
 * Know your rights when using Drupal:
   See LICENSE.txt in the same directory as this document.
 * Learn about the Drupal trademark and logo policy:
   http://drupal.com/trademark


*** Overview
---------------------------------------
A simple shopping cart that can be used in simple applications where the
ability to add products to a cart is provided. The order will be saved in the
database and sent via e-mail.

Read more on http://www.dhavyd.com/projects/shopcart


*** Requirements
---------------------------------------
- Addresses (http://drupal.org/project/addresses)
- CCK (http://drupal.org/project/cck)
- Currency API (http://drupal.org/project/currency)
- Filefield (http://drupal.org/project/filefield)
- Format Number API (http://drupal.org/project/format_number)
- Formatted Number CCK (http://drupal.org/project/formatted_number)
- Imagefield (http://drupal.org/project/imagefield)
- Money (http://drupal.org/project/money)
- Taxonomy (is already in Drupal core)
- Views (http://drupal.org/project/views)
- Views Bulk Operations (http://drupal.org/project/views_bulk_operations)


*** Installing Shopcart
---------------------------------------
- Please, make sure all required modules are installed first.
- Copy all contents of this package to your modules directory.
- Go to Administer > Site building > Modules to install this module.


*** Use Shopcart 
---------------------------------------
- Create product categories
  1. Go to Administer > Content > Taxonomy.
  2. Add terms to the vocabulary "Shopcart categories".

- Create products
  1. Go to Create content > Shopcart product.
  2. Fill all required information and save the new product.

- Administer Shopcart
  1. Go to Administer > Site configuration > Shopcart.

Read more on http://www.dhavyd.com/projects/shopcart


---------------------------------------
by Dhavyd Vanderlei - http://www.dhavyd.com