<?php
// $Id: shopcart.admin.inc,v 1.2 2011/09/09 23:31:20 dhavyd Exp $
/*
 * @file
 * shopcart.admin.inc Contains admin's settings page
 */

/**
 * Menu callback with the administration form.
 */
function shopcart_admin_general_form() {
  $form = array();
  $path = drupal_get_path('module', 'shopcart');
  $form['general_settings'] = array(
    '#value' => t("Define the messages to show when the user checks out the order."));
  $form['shopcart_show_add_to_cart_button_shopcart_product'] = array(
      '#type' => 'checkbox',
      '#title' => t("Place 'Add to Cart' link into node's links"),
      '#description' => t('You can disable this link and use your own way to add this button to page. Read simple_cart.api.inc file for more information.'),
      '#default_value' => variable_get('shopcart_show_add_to_cart_button_shopcart_product', FALSE),
  );
  $form['shopcart_admin_currency'] = array(
    '#type' => 'textfield',
    '#title' => t('Currency code to use in the shop'),
    '#default_value' => variable_get('shopcart_admin_currency', 'USD'),
    '#disabled' => FALSE,
    '#description' => t('Fill the currency code that will be used in the shop. Default is "USD".'),
    '#attributes' => array('class' => 'shopcart-admin-currency'),
    '#size' => 3,
    '#maxlength' => 3,
  );
  $form['shopcart_admin_vat'] = array(
    '#type' => 'textfield',
    '#title' => t('VAT (procent)'),
    '#default_value' => variable_get('shopcart_admin_vat', ''),
    '#disabled' => FALSE,
    '#description' => t('VAT in procent that is included in the purchase total price. Ex: for 25% VAT, fill just 25 in the field. Leave the field empty for no VAT.'),
    '#attributes' => array('class' => 'shopcart-admin-vat'),
    '#size' => 3,
    '#maxlength' => 3,
  );
  $form['shopcart_admin_shipping_fee'] = array(
    '#type' => 'textfield',
    '#title' => t('Shipping fee'),
    '#default_value' => variable_get('shopcart_admin_shipping_fee', ''),
    '#disabled' => FALSE,
    '#description' => t('Fill the shipping fee. It will be added to the total amount automatically. Leave the field empty for no shipping fee.'),
    '#attributes' => array('class' => 'shopcart-admin-shipping-fee'),
    '#size' => 5,
    '#maxlength' => 5,
  );
  $form['shopcart_admin_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin e-mail'),
    '#default_value' => variable_get('shopcart_admin_email', ''),
    '#disabled' => FALSE,
    '#description' => t('The e-mail to shop administrator. A order copy will be send to this e-mail every time a order is registred.'),
    '#attributes' => array('class' => 'shopcart-admin-email'),
  );
  $form['shopcart_admin_shop_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to the shop'),
    '#default_value' => variable_get('shopcart_admin_shop_path', ''),
    '#disabled' => FALSE,
    '#description' => t('Drupal path to the shop. Ex: "shop"'),
    '#attributes' => array('class' => 'shopcart-admin-email'),
  );
  $form['shopcart_admin_customer_service'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to the customer service of the shop'),
    '#default_value' => variable_get('shopcart_admin_customer_service', ''),
    '#disabled' => FALSE,
    '#description' => t('Fill the drupal path to the information about the customer service in your site. Ex: "customer".'),
    '#attributes' => array('class' => 'shopcart-admin-customer-service'),
  );
  // E-mail messages
  $form['shopcart_order_email_messages'] = array(
    '#type' => 'fieldset',
    '#title' => t("E-mail messages"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attributes' => array('id' => 'edit-shopcart-order-email-messages-fieldset'),
  );
  $form['shopcart_order_email_messages']['shopcart_order_email_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get('shopcart_order_email_subject', ''),
    '#disabled' => FALSE,
    '#description' => t('The subject of the e-mail confirmation message for the order.'),
    '#attributes' => array('class' => 'shopcart-order-email-subject'),
  );  
  $form['shopcart_order_email_messages']['shopcart_order_email_customer'] = array(
    '#type' => 'textarea',
    '#title' => t('Message to the customer'),
    '#default_value' => variable_get('shopcart_order_email_customer', ''),
    '#disabled' => FALSE,
    '#description' => t('This message will be sent by e-mail when the customer has confirmed the order.'),
    '#attributes' => array('class' => 'shopcart-order-email-customer'),
  );  
  $form['shopcart_order_email_messages']['shopcart_order_email_customer_foot'] = array(
    '#type' => 'textarea',
    '#title' => t('E-mail foot'),
    '#default_value' => variable_get('shopcart_order_email_customer_foot', ''),
    '#disabled' => FALSE,
    '#description' => t('This message will be set in the foot of the e-mail that will be sent to the customer.'),
    '#attributes' => array('class' => 'shopcart-order-email-customer-foot'),
  );  
  // Messages for order registration success  
  $form['shopcart_order_succeed'] = array(
    '#type' => 'fieldset',
    '#title' => t("Order registration succeed"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attributes' => array('id' => 'edit-shopcart-order-succeed-fieldset'),
  );
  $form['shopcart_order_succeed']['shopcart_order_title_succeed'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('shopcart_order_title_succeed', ''),
    '#disabled' => FALSE,
    '#description' => t('This title shows in the top of the order confirmation.'),
    '#attributes' => array('class' => 'shopcart-order-title-succeed'),
  );
  $form['shopcart_order_succeed']['shopcart_order_text_succeed'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => variable_get('shopcart_order_text_succeed', ''),
    '#disabled' => FALSE,
    '#description' => t('This text shows in the bottom of the confirmation page when the order registration succeed'),
    '#attributes' => array('class' => 'shopcart-order-introduction-succeed'),
  );
  // Messages for order registration failure
  $form['shopcart_order_fail'] = array(
    '#type' => 'fieldset',
    '#title' => t("Order registration fail"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attributes' => array('id' => 'edit-shopcart-order-fail-fieldset'),
  );
  $form['shopcart_order_fail']['shopcart_order_introduction_fail'] = array(
    '#type' => 'textarea',
    '#title' => t('Message introduction'),
    '#default_value' => variable_get('shopcart_order_introduction_fail', ''),
    '#disabled' => FALSE,
    '#description' => t('This text shows in the top of the confirmation page when the order registration succeed'),
    '#attributes' => array('class' => 'shopcart-order-introduction-fail'),
  );
  $form['shopcart_order_fail']['shopcart_order_body_fail'] = array(
    '#type' => 'textarea',
    '#title' => t('Message body'),
    '#default_value' => variable_get('shopcart_order_body_fail', ''),
    '#disabled' => FALSE,
    '#description' => t('This text shows in the bottom of the confirmation page when the order registration fail'),
    '#attributes' => array('class' => 'shopcart-order-body-fail'),
  );
  // Get product categories  
  $shopcart_vocabulary = taxonomy_get_tree(shopcart_get_vocabulary());
  if ($shopcart_vocabulary) {
    // Confirmation messages for shopcart categories
    $form['shopcart_categories'] = array(
      '#type' => 'fieldset',
      '#title' => t("Messages for product categories"),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => array('id' => 'edit-shopcart-is-cartable-' . $type . '-fieldset'),
    );
    // Messages for the product categories  
    foreach ($shopcart_vocabulary as $key => $value) {
      $form['shopcart_categories']['shopcart_category_' . $value->tid] = array(
        '#type' => 'textarea',
        '#title' => check_plain($value->name),
        '#default_value' => variable_get('shopcart_category_' . $value->tid, ''),
        '#disabled' => FALSE,
        '#description' => '',
        '#attributes' => array('class' => 'shopcart-categories shopcart-category-' . $value->tid),
      );
    }
  }
  return system_settings_form($form);
}

/**
 * Menu callback to edit an order.
 */
function shopcart_editorder_form(&$form_state, $fields) {
  // Order information
  $orderid = $fields->id;
  $buyerid = $fields->shopcart_orders_buyerid;
  $receiverid = $fields->shopcart_orders_receiverid;
  $status = $fields->shopcart_orders_status;
  $comment = $fields->shopcart_orders_comment;
  // Information about the buyer
  $buyer_firstname = $fields->shopcart_user_shopcart_orders_firstname;
  $buyer_lastname = $fields->shopcart_user_shopcart_orders_lastname;
  $buyer_company = $fields->shopcart_user_shopcart_orders_company;
  $buyer_address = $fields->shopcart_user_shopcart_orders_address;
  $buyer_zip = $fields->shopcart_user_shopcart_orders_zip;
  $buyer_postal = $fields->shopcart_user_shopcart_orders_postal;
  $buyer_country = $fields->shopcart_user_shopcart_orders_country;
  $buyer_phone = $fields->shopcart_user_shopcart_orders_phone;
  $buyer_email = $fields->shopcart_user_shopcart_orders_email;
  $message = $fields->shopcart_orders_message;
  // Information about the receiver
  $receiver_firstname = $fields->shopcart_user_shopcart_orders_1_firstname;
  $receiver_lastname = $fields->shopcart_user_shopcart_orders_1_lastname;
  $receiver_company = $fields->shopcart_user_shopcart_orders_1_company;
  $receiver_address = $fields->shopcart_user_shopcart_orders_1_address;
  $receiver_zip = $fields->shopcart_user_shopcart_orders_1_zip;
  $receiver_postal = $fields->shopcart_user_shopcart_orders_1_postal;
  $receiver_country = $fields->shopcart_user_shopcart_orders_1_country;
  $message_receiver = $fields->shopcart_orders_message_receiver;
 
  $form['editorder']['update'] = array(
    '#type' => 'submit',
    '#name' => 'update_order',
    '#value' => t('Update'),
  );
  $form['editorder']['orderid'] = array(
    '#type' => 'textfield',
    '#title' => t("Order ID"),
    '#required' => TRUE,
    '#disabled' => TRUE,
    '#value' => $orderid,
  );
  
  // Order fields
  $form['editorder']['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => array(
      '0' => t('Not paid'),
      '1' => t('Paid'),
      '2' => t('Delivered'),
    ),
    '#required' => TRUE,
    '#default_value' => $status,
  );

  $form['editorder']['comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Comments from') . ' ' . variable_get('site_name', ''),
    '#default_value' => $comment,
  );
  
  // Form buyer
  $form['editorder']['buyer_firstname'] = array(
    '#type' => 'textfield',
    '#title' => t("Buyer's firstname"),
    '#required' => TRUE,
    '#default_value' => $buyer_firstname,
  );
  $form['editorder']['buyer_lastname'] = array(
    '#type' => 'textfield',
    '#title' => t("Buyer's lastname"),
    '#required' => TRUE,
    '#default_value' => $buyer_lastname,
  );
  $form['editorder']['buyer_company'] = array(
    '#type' => 'textfield',
    '#title' => t("Buyer's company"),
    '#required' => FALSE,
    '#default_value' => $buyer_company,
  );
  $form['editorder']['buyer_address'] = array(
    '#type' => 'textfield',
    '#title' => t("Buyer's address"),
    '#required' => TRUE,
    '#default_value' => $buyer_address,
  );
  $form['editorder']['buyer_zip'] = array(
    '#type' => 'textfield',
    '#title' => t("Buyer's zip code"),
    '#required' => TRUE,
    '#default_value' => $buyer_zip,
  );
  $form['editorder']['buyer_postal'] = array(
    '#type' => 'textfield',
    '#title' => t("Buyer's postal address"),
    '#required' => TRUE,
    '#default_value' => $buyer_postal,
  );
  $form['editorder']['buyer_country'] = array(
    '#type' => 'textfield',
    '#title' => t("Buyer's country"),
    '#required' => TRUE,
    '#default_value' => $buyer_country,
  );
  $form['editorder']['buyer_phone'] = array(
    '#type' => 'textfield',
    '#title' => t("Buyer's telephone number"),
    '#required' => TRUE,
    '#default_value' => $buyer_phone,
  );
  $form['editorder']['buyer_email'] = array(
    '#type' => 'textfield',
    '#title' => t("Buyer's e-mail"),
    '#required' => TRUE,
    '#default_value' => $buyer_email,
  );
  $form['editorder']['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message to') . ' ' . variable_get('site_name', 'shop'),
    '#default_value' => $message,
  );
  // Form receiver
  $form['editorder']['receiver_firstname'] = array(
    '#type' => 'textfield',
    '#title' => t("Receiver's firstname"),
    '#required' => FALSE,
    '#default_value' => $receiver_firstname,
  );
  $form['editorder']['receiver_lastname'] = array(
    '#type' => 'textfield',
    '#title' => t("Receiver's lastname"),
    '#required' => FALSE,
    '#default_value' => $receiver_lastname,
  );
  $form['editorder']['receiver_company'] = array(
    '#type' => 'textfield',
    '#title' => t("Receiver's company"),
    '#required' => FALSE,
    '#default_value' => $receiver_company,
  );
  $form['editorder']['receiver_address'] = array(
    '#type' => 'textfield',
    '#title' => t("Receiver's address"),
    '#required' => FALSE,
    '#default_value' => $receiver_address,
  );
  $form['editorder']['receiver_zip'] = array(
    '#type' => 'textfield',
    '#title' => t("Receiver's zip code"),
    '#required' => FALSE,
    '#default_value' => $receiver_zip,
  );
  $form['editorder']['receiver_postal'] = array(
    '#type' => 'textfield',
    '#title' => t("Receiver's postal address"),
    '#required' => FALSE,
    '#default_value' => $receiver_postal,
  );
  $form['editorder']['receiver_country'] = array(
    '#type' => 'textfield',
    '#title' => t("Receiver's country"),
    '#required' => FALSE,
    '#default_value' => $receiver_country,
  );
  $form['editorder']['message_receiver'] = array(
    '#type' => 'textarea',
    '#title' => t('Message to receiver'),
    '#default_value' => $message_receiver,
  );
  // Hidden fields  
  $form['editorder']['buyerid'] = array('#type' => 'hidden', '#value' => $buyerid);
  $form['editorder']['receiverid'] = array('#type' => 'hidden', '#value' => $receiverid);
  // Redirect URL  
  $form['#redirect'] = 'admin/reports/shopcart';

  return $form;
}

/**
 * Form validation for order edit.
 */
function shopcart_editorder_form_validate($form, $form_state) {
  $op = $form_state['clicked_button']['#name'];
  switch ($op) {
    case 'back_to_cart':
      drupal_goto('shopcart');
      break;
  }
}

/**
 * Submit for order edit.
 */
function shopcart_editorder_form_submit($form, $form_state) {
  $op = $form_state['clicked_button']['#name'];
  switch ($op) {
    case 'update_order':
      $values = $form_state['values'];
      // Order information
      $orderid = $values['orderid'];
      $buyerid = $values['buyerid'];
      $receiverid = $values['receiverid'];
      // Variables to update the table shopcart_orders
      $message = $values['message'];
      $message_receiver = $values['message_receiver'];
      $status = $values['status'];
      $comment = $values['comment'];
      // Variables to update the table shopcart_users (buyer)
      $buyer_firstname = $values['buyer_firstname'];
      $buyer_lastname = $values['buyer_lastname'];
      $buyer_company = $values['buyer_company'];
      $buyer_address = $values['buyer_address'];
      $buyer_zip = $values['buyer_zip'];
      $buyer_country = $values['buyer_country'];
      $buyer_postal = $values['buyer_postal'];
      $buyer_phone = $values['buyer_phone'];
      $buyer_email = $values['buyer_email'];
      // Variables to update the table shopcart_users (receiver)
      $receiver_firstname = $values['receiver_firstname'];
      $receiver_lastname = $values['receiver_lastname'];
      $receiver_company = $values['receiver_company'];
      $receiver_address = $values['receiver_address'];
      $receiver_zip = $values['receiver_zip'];
      $receiver_postal = $values['receiver_postal'];
      // Update the table shopcart_orders
      $sql_order = "UPDATE {%s} SET message = '%s', message_receiver = '%s', status = %d, comment = '%s' WHERE id = %d";
      $result_order = db_query($sql_order, SHOPCART_ORDERS_TABLE_NAME, check_plain($message), check_plain($message_receiver), check_plain($status), check_plain($comment), check_plain($orderid));
      // Update the table shopcart_user (buyer)
      $sql_buyer = "UPDATE {%s} SET firstname = '%s', lastname = '%s', company = '%s', address = '%s', zip = '%s',
        postal = '%s', country = '%s', phone = '%s', email = '%s' WHERE id = %d";
      $result_buyer = db_query($sql_buyer, SHOPCART_USER_TABLE_NAME,
          check_plain($buyer_firstname), check_plain($buyer_lastname), check_plain($buyer_company), check_plain($buyer_address),
          check_plain($buyer_zip), check_plain($buyer_postal), check_plain($buyer_country), check_plain($buyer_phone),
          check_plain($buyer_email), check_plain($buyerid));
      // Update the table shopcart_user (receiver)
      $sql_receiver = "UPDATE {%s} SET firstname = '%s', lastname = '%s', company = '%s', address = '%s', zip = '%s',
        postal = '%s', country = '%s' WHERE id = %d";
      $result_receiver = db_query($sql_receiver, SHOPCART_USER_TABLE_NAME,
          check_plain($receiver_firstname), check_plain($receiver_lastname), check_plain($receiver_company), check_plain($receiver_address),
          check_plain($receiver_zip), check_plain($receiver_postal), check_plain($receiver_country), check_plain($receiverid));
      // Confirmation message
      drupal_set_message(check_plain(t('The order "' . $orderid . '" was updated!')));
      break;
  }
}

/**
 * Returns a list with all registred orders
 */
function shopcart_order_list() {
  return views_embed_view('Shopcart_orders', 'default');
}



