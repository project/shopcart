<?php
// $Id: shopcart.views.inc,v 1.2 2011/09/26 15:36:12 dhavyd Exp $
/*
 * @file
 * shopcart.views.inc 
 */

/**
 * Implementation of hook_views_data().
 *
 * @return array
 */
function shopcart_views_data() {
  /*
   * Table shopcart_order_items
   */
  $data['shopcart_order_items']['table']['group'] = t('Shopcart order items');
  $data['shopcart_order_items']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Shopcart order items'),
    'help' => t("Handle items in a specific order."),
    'weight' => -10,
  );

  $data['shopcart_order_items']['id'] = array(
    'title' => t('ID'),
    'help' => t("Display order item ID."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['shopcart_order_items']['orderid'] = array(
    'title' => t('Order ID'),
    'help' => t("Order ID."),
    'relationship' => array(
      'base' => 'shopcart_orders',
      'base field' => 'id',
      'handler' => 'views_handler_relationship', 
      'label' => t("Order ID"),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
       'handler' => 'views_handler_filter_numeric',
    ),
  );
  
  $data['shopcart_order_items']['productid'] = array(
    'title' => t('Product ID'),
    'help' => t("Display product id."),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship', 
      'label' => t("Product ID"),
    ),
  );
  
  $data['shopcart_order_items']['price'] = array(
    'title' => t('Price'),
    'help' => t("Display product price."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['shopcart_order_items']['quantity'] = array(
    'title' => t('Quantity'),
    'help' => t("Quantity of products."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['shopcart_order_items']['params'] = array(
    'title' => t('Params'),
    'help' => t("Special params."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  
  // ----------
  
  /*
   * Table shopcart_user
   */
  $data['shopcart_user']['table']['group'] = t('Shopcart user');
  $data['shopcart_user']['id'] = array(
    'title' => t('ID'),
    'help' => t("Display user's ID."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['shopcart_user']['email'] = array(
    'title' => t('E-mail'),
    'help' => t("User's e-mail."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['shopcart_user']['firstname'] = array(
    'title' => t('Firstname'),
    'help' => t("Display user's firstname."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
       'handler' => 'views_handler_filter_string',
    ),
  );
  $data['shopcart_user']['lastname'] = array(
    'title' => t('Lastname'),
    'help' => t("Display user's lastname."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
       'handler' => 'views_handler_filter_string',
    ),
  );

  $data['shopcart_user']['company'] = array(
    'title' => t('Company'),
    'help' => t("Company."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['shopcart_user']['address'] = array(
    'title' => t('Address'),
    'help' => t("Address, street, number."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['shopcart_user']['zip'] = array(
    'title' => t('Zip'),
    'help' => t("The zip code."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['shopcart_user']['postal'] = array(
    'title' => t('Postal'),
    'help' => t("Postal address."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  
  $data['shopcart_user']['country'] = array(
    'title' => t('Country'),
    'help' => t("Country."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  
  $data['shopcart_user']['phone'] = array(
    'title' => t('Phone'),
    'help' => t("Telephone number."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  
  $data['shopcart_user']['usertype'] = array(
    'title' => t('Usertype'),
    'help' => t("Usertype."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  
  // ----------
  
  /*
   * Table shopcart_orders
   */
  $data['shopcart_orders']['table']['group'] = t('Shopcart orders');
  $data['shopcart_orders']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Shopcart orders'),
    'help' => t("Handle orders that creates by the module Shopcart."),
    'weight' => -10,
  );
  
  $data['shopcart_orders']['id'] = array(
    'title' => t('ID'),
    'help' => t('Display order ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
       'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['shopcart_orders']['buyerid'] = array(
    'title' => t("The buyer's ID"),
    'help' => t("Display the buyer's ID."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
      'base' => 'shopcart_user',
      'base field' => 'id',
      'handler' => 'views_handler_relationship', 
      'label' => t("Buyer's ID"),
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
       'handler' => 'views_handler_filter_string',
    ),
  );

  $data['shopcart_orders']['receiverid'] = array(
    'title' => t("The receiver's ID"),
    'help' => t("Display the receiver's ID."),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
      'base' => 'shopcart_user',
      'base field' => 'id',
      'handler' => 'views_handler_relationship', 
      'label' => t("Receiver's ID"),
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
       'handler' => 'views_handler_filter_string',
    ),
  );
  
  $data['shopcart_orders']['message'] = array(
    'title' => t('Message'),
    'help' => t('Order message.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
  );
  
  $data['shopcart_orders']['message_receiver'] = array(
    'title' => t('Message to receiver'),
    'help' => t('Message to receiver') . '.',
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
  );
  
  $data['shopcart_orders']['datetime'] = array(
    'title' => t('Datetime'),
    'help' => t('Date and time the order was created.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
       'handler' => 'views_handler_filter_date',
    ),
  );

  $data['shopcart_orders']['status'] = array(
    'title' => t('Status'),
    'help' => t('Order status.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
       'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['shopcart_orders']['comment'] = array(
    'title' => t('Comments'),
    'help' => t('Comments to this order.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
       'handler' => 'views_handler_filter_string',
    ),
  );
  // ----------
  
  return $data;
}





