<?php
// $Id: shopcart.admin.inc,v 1.2 2011/09/09 23:31:20 dhavyd Exp $
/*
 * @file
 * shopcart.admin.inc Contains admin's settings page
 */

/**
 * Get plugins.
 */
function shopcart_get_plugins() {
  $shopcart_plugins = array('date', 'node', '');
  return $shopcart_plugins;
}

/**
 * Gets values for a specific plugin.
 */
function shopcart_plugins_func_get_value($key, $value) {
  $output = array();
  // Get plugins
  $shopcart_plugins = shopcart_get_plugins();
  if (in_array($key, $shopcart_plugins)) {
    $func_name = 'shopcart_plugins_' . $key . '_get';
    // If the function exists, the returned values will be printed
    if (function_exists($func_name)) {
      $func_output = call_user_func($func_name, $value);
      $output[$key] = array(
        'title' => $func_output['title'],
        'value' => $func_output['value'],
      );
    }
    elseif (($key == 'price') && is_numeric($value)) {
      $output[$key] = array(
        'title' => t('Price'),
        'value' => $value,
      );
    }
  }
  return $output;
}

/**
 * Payment-plugin.
 */
function shopcart_plugins_payment_get($value) {
  $payment_module_path = drupal_get_path('module', 'misc') . '/payment';
  include($payment_module_path . '/payment.php');
  return payment_register($value);
}

/**
 * Date-plugin: set date string to UNIX timestamp.
 */
function shopcart_plugins_date_set($value) {
  $output = array();
  $output = array(
    'title' => t('Date'),
    'value' => strtotime($value),
  );
  return $output;
}

/**
 * Date-plugin: set UNIX timestamp to date string in format YYYY-MM-DD.
 */
function shopcart_plugins_date_get($value) {
  $output = array();
  $output = array(
    'title' => t('Date'),
    'value' => date("Y-m-d", $value),
  );
  return $output;
}

/**
 * Node-plugin: get content type and title of the node.
 */
function shopcart_plugins_node_get($value) {
  $output = array();
  $node = node_load($value);
  $node_types = node_get_types();
  $output = array(
    'title' => $node_types[$node->type]->name,
    'value' => $node->title,
  );
  return $output;  
}


